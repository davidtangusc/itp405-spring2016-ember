import Ember from 'ember';

export default Ember.Route.extend({
  model(params) {
    return $.getJSON(`http://api.github.com/users/${params.username}`);
  },
  afterModel(user) {
    return $.getJSON(user.repos_url).then(function(repos) {
      user.repos = repos;
    });
  }
});
