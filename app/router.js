import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('faq');
  this.route('users', function() {
    this.route('user', { path: ':username' });
  });
});

export default Router;
